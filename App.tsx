import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, Button } from 'react-native';
import Constants from 'expo-constants';

import { BUILD_COMMIT, BUILD_AUTHOR, BUILD_DATE } from './src/env';
import useExpoUpdates from './src/useExpoUpdates';

export default function App() {
  const { isAvailable, lastEvent, reload } = useExpoUpdates();
  return (
    <SafeAreaView style={styles.container}>
      <View style={[styles.center, { flex: 1 }]}>
        <Text>Demo app</Text>
        <Text>Release channel: {Constants.manifest.releaseChannel}</Text>
        <Text>git commit: {BUILD_COMMIT}</Text>
        <Text>published by: {BUILD_AUTHOR}</Text>
        <Text>build date: {BUILD_DATE}</Text>
        <Text>Update available: {isAvailable ? 'yes' : 'no'}</Text>
        {isAvailable && <Button onPress={reload} title="Reload"></Button>}
      </View>
      <View style={styles.center}>
        {lastEvent && (
          <Text>
            {lastEvent.type === 'downloadStart'
              ? 'Download started...'
              : lastEvent.type === 'downloadProgress'
              ? 'Download update ...'
              : lastEvent.type === 'downloadFinished'
              ? 'Update ready'
              : lastEvent.type === 'error'
              ? `Error: ${lastEvent.message}`
              : lastEvent.type === 'noUpdateAvailable'
              ? 'No Update available'
              : `Unknown type: "${lastEvent.type}"`}
          </Text>
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
