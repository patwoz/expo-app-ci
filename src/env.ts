export const BUILD_COMMIT = process.env.BUILD_COMMIT;
export const BUILD_AUTHOR = process.env.BUILD_AUTHOR;
export const BUILD_DATE = process.env.BUILD_DATE;
