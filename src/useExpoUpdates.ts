import React from 'react';
import { Updates } from 'expo';
import { AppManifest } from 'expo-constants/build/Constants.types';

export type UpdatesEventTypes =
  | 'downloadStart'
  | 'downloadProgress'
  | 'noUpdateAvailable'
  | 'downloadFinished'
  | 'error';

export interface UpdatesEvent {
  type: UpdatesEventTypes;
  manifest?: AppManifest;
  message?: string;
}

export interface UseExpoUpdatesState {
  lastEvent?: UpdatesEvent;
  isAvailable?: boolean;
  checkForUpdate: () => Promise<boolean>;

  reload: () => Promise<void>;
}

function useExpoUpdates(): UseExpoUpdatesState {
  const [lastEvent, setLastEvent] = React.useState<UpdatesEvent>();
  const [isAvailable, setIsAvailable] = React.useState<boolean>(null);

  async function checkForUpdate() {
    const update = await Updates.checkForUpdateAsync();
    setIsAvailable(update.isAvailable);
    return update.isAvailable;
  }

  React.useEffect(() => {
    checkForUpdate();
  }, []);

  React.useEffect(() => {
    function updateListener(event: UpdatesEvent) {
      setLastEvent(event);
    }

    const subscription = Updates.addListener(updateListener);
    return () => {
      subscription.remove();
    };
  }, []);

  return {
    lastEvent,

    reload: Updates.reload,

    isAvailable,
    checkForUpdate,
  };
}

export default useExpoUpdates;
